package com.example.esmecalendar

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.wifi.ScanResult
import android.net.wifi.WifiManager
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.TextView
import com.example.esmecalendar.interfaces.ILocalisation
import com.example.esmecalendar.mocks.LocationMock3
import kotlinx.android.synthetic.main.activity_location.*


class LocationActivity : AppCompatActivity() {

    var locationIterface: ILocalisation = LocationMock3()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location)

        startLocalisation()

        wifi_list.setOnClickListener { view ->
            Log.v("TAG", "coucou");
            val intent = Intent(applicationContext, RttWifiActivity::class.java)

            startActivity(intent)

        }
        scan_location.setOnClickListener { view ->
            Log.v("TAG", "scan_location");
            registerReceiver(broadcastReceiver, IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION))
            wifiManager.startScan();

        }

        change_etage.setOnClickListener { view ->
            Log.v("TAG", "change etage");
            if(etage == 0){
                etage0.visibility = View.GONE;
                etage1.visibility = View.VISIBLE
                etage = 1
                change_etage.text = "Etage 0"
            }else{
                etage1.visibility = View.GONE;
                etage0.visibility = View.VISIBLE
                etage = 0
                change_etage.text = "Etage 1"
            }

        }
        wifiManager = this.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager


        var array =resources.getStringArray(R.array.liste_salle)
        for (arg in array){
            var id = getResources().getIdentifier(arg, "id", getPackageName())
            findViewById<TextView>(id).setBackgroundResource(R.drawable.class_bg)
        }
        var array1 =resources.getStringArray(R.array.list_couloir)
        for (arg in array1){
            var id = getResources().getIdentifier(arg, "id", getPackageName())
            findViewById<TextView>(id).setBackgroundResource(R.drawable.couloir_bg)
        }

    }
    var etage = 0;




    var resultList = ArrayList<ScanResult>()
    lateinit var wifiManager: WifiManager
    val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            resultList = wifiManager.scanResults as ArrayList<ScanResult>
            Log.d("TESTING", "onReceive Called : " +resultList.size)
            unregisterReceiver(this)
            if(resultList != null && resultList.size > 0){
                var lowerMac = resultList.get(0).BSSID;
                var lowerLevel = resultList.get(0).level;
                for (arg in resultList){
                    if(arg.level > lowerLevel){
                        lowerMac = arg.BSSID;
                        lowerLevel = arg.level;
                    }
                }
                // afficher salle
                var array =resources.getStringArray(R.array.liste_salle)
                for (arg in array){
                    var id = getResources().getIdentifier(arg, "id", getPackageName())
                    findViewById<TextView>(id).setBackgroundResource(R.drawable.class_bg)
                }
                var array1 =resources.getStringArray(R.array.list_couloir)
                for (arg in array1){
                    var id = getResources().getIdentifier(arg, "id", getPackageName())
                    findViewById<TextView>(id).setBackgroundResource(R.drawable.couloir_bg)
                }

                // cahngement de distance entre salle
                if (lowerLevel > -65){
                    val sharedPref = getSharedPreferences("MES_PREF", Context.MODE_PRIVATE) ?: return
                    val salle = sharedPref.getString(lowerMac, "")
                    if(salle.equals("")){
                        //error inconnu
                        Snackbar.make(content, "Adresse mac inconnue", Snackbar.LENGTH_LONG).show()
                    }else{
                        // changer couleur
                        Snackbar.make(content, "Adresse mac connue " + salle, Snackbar.LENGTH_LONG).show()


                        var id = getResources().getIdentifier(salle, "id", getPackageName())
                        findViewById<TextView>(id).setBackgroundResource(R.drawable.class_bg_selected)
                    }
                }else
                    for (arg in array1){
                        var id = getResources().getIdentifier(arg, "id", getPackageName())
                        findViewById<TextView>(id).setBackgroundResource(R.drawable.couloir_bg_selected)
                    }


            }else{
                //error no wifi
                Snackbar.make(content, "Aucun point wifi détecté", Snackbar.LENGTH_LONG).show()
            }
        }
    }


    fun startLocalisation() {

        Thread {

            var classroom: String? = ""

            while (true) {
                classroom = locationIterface
                    .getPositionFromWifiList(locationIterface.getWifiList())

                Thread.sleep(1000)

                runOnUiThread {
                    textView.text = classroom
                }

            }


        }.start()


    }



}
